Scancode-forward 
================

Purpose
-------

Waits on a tty in search for input. Splits said input in separated records, sends them to a remote host.
Handles connection and reconnection to the remote host as deem necessary in order to always maintain the channel viable.
The typical usage scenario involves the use of *code scanners, which qualify themselves as keyboard hardware, send a number of keystrokes as the scanned code mandates, followed by a configurable end-of-record character.

Prerequisites
-------------

* Any systemd-based Linux system (instructions are for Debian derivatives, Raspbian, Ubuntu, etc)
* Python 2.7
* any other required library should be auto-pulled during the install phase

Install
-------

* install/prepare the host Linux system as adequate
* make sure graphical UI (Xorg, others) is *NOT* started as boot. If it does, disable it. Server or lite Linux distro normally suit this scenario better
* command: apt-get install python-pip git libncurses-dev build-essential
* command: pip install git+https://gitlab.com/neta_sm/scancode_forward.git

At this point the service should be installed and running, reboot-persistant.

* Configure the contents of /etc/scancode_forward.conf to suit your needs (remote host, port, etc).
* restart the system or issue the command: systemctl restart scancode_forward.service

Usage and options
-----------------

The ``scancode_forward.py`` command should be installed and runnable.
Running it with the ``--help`` prints a small usage manual.

The options that can be configured inside /etc/scancode_forward.conf are the same which are documented via ``--help``
