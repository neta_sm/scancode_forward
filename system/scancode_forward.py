#!/usr/bin/env python2.7

from __future__ import print_function

from threading import Thread

import urwid.widget
from datetime import timedelta, datetime

from tornado.escape import utf8
from tornado.gen import coroutine, with_timeout, TimeoutError
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.iostream import IOStream, StreamClosedError
from tornado.tcpclient import TCPClient
from tornado.options import define, options, parse_command_line, parse_config_file
from subprocess import call, check_output
import urwid.curses_display
import urwid.raw_display
import urwid.web_display
import urwid
from time import time


define("host", type=str, help="ip or hostname to connect to")
define("port", type=int, help="TCP port number")
define("debug", type=bool, help="enable debug, allow exit using 'Q'", default=False)
define("record_separator_in", type=str, default=",", help="input record separator "
                                                   "(specified as hex char or string)")
define("record_separator_out", type=str, default="0x0a", help="record separator sent to the host "
                                                       "(specified as hex char or string)")
define("field_separator_out", type=str, default=":", help="field separator sent to the host "
                                                       "(specified as hex char or string)")
define("config", help="use options from config file", callback=lambda x: parse_config_file(x, final=False))
define("title", help="software name on title bar", default="CODE SCAN")

ioloop = IOLoop.instance()

ioloop_add_handler_old = ioloop.add_handler



def ioloop_add_handler_new(*a, **ka):
    try:
        return ioloop_add_handler_old(*a, **ka)
    except IOError:
        return

ioloop.add_handler = ioloop_add_handler_new
quiet_exceptions = (TimeoutError, StreamClosedError)

def parse_separator(s):
    if s.find("0x") == 0:
        return "%c" % int(s, 16)
    return s


class ConnectionManager(object):
    IDLE="IDLE"
    CONNECTING="CONNECTING"
    CONNECTED="CONNECTED"
    DISCONNECTED="DISCONNECTED"

    def __init__(self, host, port, ioloop=None):
        if ioloop is None:
            ioloop = IOLoop.current()
        self._ioloop = ioloop
        self._status = self.IDLE
        self._host = host
        self._port = port
        self._client = TCPClient()
        self._connect_timeout = timedelta(seconds=3)
        self._write_timeout = timedelta(seconds=3)
        self._reconnect_delay = timedelta(seconds=3)
        self._stream = None
        self._status_change_listeners = set()

    def connect(self):
        self._ioloop.add_callback(self._do_connect)

    @property
    def status_str(self):
        if self._status == self.IDLE:
            return "IDLE"
        if self._status == self.DISCONNECTED:
            return "%s from %s:%r" % (self._status, self._host, self._port)
        return "%s to %s:%r" % (self._status, self._host, self._port)

    @coroutine
    def _do_connect(self):
        self._status = self.CONNECTING
        self._emit_status_change()
        connection_task = self._client.connect(self._host, self._port)
        connection_attempt = with_timeout(self._connect_timeout, connection_task, quiet_exceptions=quiet_exceptions)
        try:
            self._stream = yield connection_attempt
            assert isinstance(self._stream, IOStream)
            self._status = self.CONNECTED
            self._emit_status_change()
            self._stream.set_nodelay(True)
            self._ioloop.add_callback(self._read_data_and_discard)
        except:
            self._ioloop.call_later(self._reconnect_delay.total_seconds(), self._do_connect)

    def _on_close(self):
        self._stream = None
        self._status = self.DISCONNECTED
        self._emit_status_change()
        self._ioloop.add_callback(self._do_connect)

    @coroutine
    def _write_coro(self, data):
        if self._stream is None:
            return
        data = utf8(data)
        write_task = self._stream.write(data)
        write_attempt = with_timeout(self._write_timeout, write_task, quiet_exceptions=quiet_exceptions)
        try:
            yield write_attempt
        except:
            self._on_close()

    def write(self, data):
        self._ioloop.add_callback(self._write_coro, data)

    def subscribe_status_change(self, cback):
        self._status_change_listeners.add(cback)

    def _emit_status_change(self):
        for cback in self._status_change_listeners:
            try:
                cback(self)
            except:
                pass

    @coroutine
    def _read_data_and_discard(self):
        # not really useful, but discards any inbound data anyway,
        # and alerts us of closed sockets in real time
        try:
            while True:
                yield self._stream.read_bytes(1024)
        except IOError:
            self._on_close()




Screen = urwid.curses_display.Screen


class App(object):
    def __init__(self, conn_manager):
        self.screen = Screen()
        self.header_txt = urwid.Text("")
        header = urwid.AttrWrap(self.header_txt, 'header')
        self.lw = urwid.SimpleListWalker([])
        self.listbox = urwid.ListBox(self.lw)
        self.listbox = urwid.AttrWrap(self.listbox, 'listbox')
        self.top = urwid.Frame(self.listbox, header)
        self.buf = ""
        self.separator_in = parse_separator(options.record_separator_in)
        self.separator_out = parse_separator(options.record_separator_out)
        self.field_separator_out = parse_separator(options.field_separator_out)

        self.conn_manager = conn_manager
        self.conn_manager.subscribe_status_change(self.update_header)
        self.ctr = int(time())
        self.loop = None

        self.lw.append(
            urwid.Text(""))
        self.lw.append(
            urwid.Columns([
                ('weight', 2, urwid.Text("TIMESTAMP")),
                urwid.Text("CODE"), ]
            ))
        self.lw.append(
            urwid.Text(""))

    def title(self):
        return "%s ---- %s" % (options.title, check_output("date").strip())

    def status(self):
        return self.conn_manager.status_str

    def input_filter(self, keys, raw):
        if options.debug:
            if 'q' in keys or 'Q' in keys:
                raise urwid.ExitMainLoop

        t = []
        for k in keys:
            self.buf += k
            if self.buf.endswith(self.separator_in):
                code = self.buf[0:-len(self.separator_in)]
                if code:
                    self.post_code(code)
                self.buf = ""

        return keys

    def run(self):
        self.loop = urwid.MainLoop(self.top, [
            ('header', 'black', 'dark cyan', 'standout'),
            ('key', 'yellow', 'dark blue', 'bold'),
            ('listbox', 'light gray', 'black'),
        ], self.screen, input_filter=self.input_filter)

        try:
            old = self.screen.tty_signal_keys('undefined', 'undefined',
                                         'undefined', 'undefined', 'undefined')
            self.loop.run()
        finally:
            self.screen.tty_signal_keys(*old)

    def post_code(self, code):
        rawt = urwid.Text(code)
        self.lw.append(
            urwid.Columns([
                ('weight', 2, urwid.Text(self.timestamp())),
                rawt])
        )
        self.listbox.set_focus(len(self.lw) - 1, 'above')
        max_rows = 20
        while len(self.lw) > max_rows:
            self.lw.pop(3)
        self.conn_manager.write("%s%s%s%s" % (
            "%d" % self.ctr,
            self.field_separator_out,
            code,
            self.separator_out
        ))

    def timestamp(self):
        self.ctr += 1
        return "%s.%08d" % (datetime.now().strftime("%Y%m%d%H%M%S"), self.ctr)

    def update_header(self, *a, **ka):
        self.header_txt.set_text("%s  ----  %s" % (
            self.title(),
            self.status(),
        ))
        if self.loop is not None:
            self.loop.draw_screen()


def main():
    parse_command_line()
    call(["setterm", "-blank", "0"]) # disable console tty inactivity blanking
    cm = ConnectionManager(options.host, options.port, ioloop=ioloop)
    if options.host and options.port:
        cm.connect()
    conn_thread = Thread(target=ioloop.start)
    conn_thread.start()
    app = App(conn_manager=cm)
    pcb = PeriodicCallback(app.update_header, 1000, io_loop=ioloop)
    pcb.start()
    app.run()
    ioloop.stop()
    conn_thread.join()


if __name__ == '__main__':
    main()
