#!/usr/bin/env python
from subprocess import check_call

PACKAGE_NAME="scancode_forward"
PACKAGE_ROOT="scancode_forward"
PACKAGE_URL="http://www.roboticom.it"
VERSION="0.0.4"
DESCRIPTION="Scancode forward console agent"
AUTHOR_NAME="Andrea Gronchi"
AUTHOR_EMAIL="agronchi@smrobotica.it"
TEST_SUITE="tests"
INSTALL_REQUIRES = open("requirements.txt").readlines()
CLASSIFIERS = [
    "Development Status :: 5 - Mature",
    "Intended Audience :: Developers",
    "License :: Other/Proprietary License",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: Implementation :: CPython",
]
SCRIPTS=[
    "system/scancode_forward.py",
]

try:
    from ez_setup import use_setuptools
    use_setuptools()
except ImportError:
    pass

from setuptools import setup, find_packages
import sys, os

from setuptools.command.install import install as InstallCommand


class CustomInstall(InstallCommand):
    def run(self):
        InstallCommand.run(self)
        check_call(["sh", "install_systemd_service.sh"])

tests_require = []
other_commands = {
    "install": CustomInstall,
}

if "coverage" in sys.argv:
    tests_require += [
        "coverage",
    ]

if "tox" in sys.argv:
    tests_require += [
        "tox",
        "setuptools",
        "virtualenv",
    ],


def namespace_packages():
    result = []
    toks = PACKAGE_ROOT.split(".")
    for i in range(1, len(toks)):
        result.append(".".join(toks[:i]))
    return result


setup(
    name            = PACKAGE_NAME,
    version         = VERSION,
    description     = DESCRIPTION,
    author          = AUTHOR_NAME,
    author_email    = AUTHOR_EMAIL,
    url             = PACKAGE_URL,
    zip_safe = False, # unnecessary; it avoids egg-as-zipfile install
    packages = find_packages(exclude=['tests']),
#    namespace_packages = namespace_packages(),
    install_requires = INSTALL_REQUIRES,
    tests_require = tests_require,
    cmdclass = other_commands,
    classifiers = CLASSIFIERS,
    test_suite=TEST_SUITE,
    scripts=SCRIPTS
)
