#!/bin/sh


install_file()
{
    src=$1
    dest=$2
    overwrite=$3
    fn=`basename $src`
    dest_fn="$dest/$fn"
    if ! test -f $src
    then
        echo " **** file not found: $src"
        exit 1
    fi
    if ! test -d $dest
    then
        echo " **** destination not found: $dest"
        exit 1
    fi

    if test -e "$dest_fn" && test x$overwrite = xyes
    then
        echo " **** destination exists, deleting old $dest_fn"
        rm -f "$dest_fn"
    fi

    echo "installing $src to $dest"
    if ! test -e "$dest_fn"
    then
        if ! cp $src $dest
        then
            echo " **** unable to install source file $src to destination $dest"
            exit 1
        fi
    fi
    echo "changing ownership of $dest_fn to root user"
    if ! chown root $dest_fn
    then
        echo " **** unable to change ownership of $dest_fn to root user"
        exit 1
    fi
    echo "changing permissions for $dest_fn"
    if ! chmod 644 $dest_fn
    then
        echo " **** unable to change permissions for $dest_fn"
        exit 1
    fi
}



install_file system/scancode_forward.service /etc/systemd/system yes
install_file conf/scancode_forward.conf /etc

echo "reloading SystemD"
systemctl daemon-reload
echo "starting SystemD scancode_forward.service"
systemctl start scancode_forward.service
systemctl enable scancode_forward.service
sleep 3

systemctl status scancode_forward.service
